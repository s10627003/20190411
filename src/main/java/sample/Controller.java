package sample;

import com.company.myresume.MyresumeApplication;
import com.company.myresume.MyresumeApplicationBuilder;
import com.company.myresume.myresume.myresume.member.Member;
import com.company.myresume.myresume.myresume.member.MemberImpl;
import com.company.myresume.myresume.myresume.member.MemberManager;
import com.speedment.runtime.core.ApplicationBuilder;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Controller {
    public Button btnAdd;
    public TextField txtMobile;
    public TextField txtdel;
    public Button btnDel;

    public void onAdd(ActionEvent actionEvent) {
        String  mobile = txtMobile.getText();
        if (mobile.length()==10) {
            MyresumeApplication myresumeApplication = new MyresumeApplicationBuilder()
                    .withUsername("root").withPassword("abcd1234")
                    .withLogging(ApplicationBuilder.LogType.CONNECTION)
                    .withLogging(ApplicationBuilder.LogType.PERSIST)
                    .withLogging(ApplicationBuilder.LogType.REMOVE)
                    .build();

            MemberManager memberManager = myresumeApplication.getOrThrow(MemberManager.class);

            Member member = new MemberImpl().setName("笨蛋").setMobile("0987888777").setBrithplace("台灣")
                    .setBirthday(Date.valueOf("1999-08-07"));

            memberManager.persist(member);
        }else{
            System.out.println("不等於10");
        }
    }

    public void onDel(ActionEvent actionEvent) {
        String  mobile = txtdel.getText();
        MyresumeApplication myresumeApplication = new MyresumeApplicationBuilder()
                .withUsername("root").withPassword("abcd1234")
                .withLogging(ApplicationBuilder.LogType.CONNECTION)
                .withLogging(ApplicationBuilder.LogType.PERSIST)
                .withLogging(ApplicationBuilder.LogType.REMOVE)
                .build();

        MemberManager memberManager = myresumeApplication.getOrThrow(MemberManager.class);

        List<Member> memberList = memberManager.stream().filter(Member.MOBILE.equal(mobile)).collect(Collectors.toList());

        memberList.forEach(
                memberManager::remove
        );


    }
}
