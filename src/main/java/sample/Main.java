package sample;

import com.company.myresume.MyresumeApplication;
import com.company.myresume.MyresumeApplicationBuilder;
import com.company.myresume.myresume.myresume.member.Member;
import com.company.myresume.myresume.myresume.member.MemberImpl;
import com.company.myresume.myresume.myresume.member.MemberManager;
import com.speedment.runtime.core.ApplicationBuilder;
import com.speedment.runtime.core.exception.SpeedmentException;
import com.speedment.runtime.core.manager.Manager;
import com.speedment.runtime.core.manager.ManagerConfigurator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();


    }


    public static void main(String[] args) {
        launch(args);
    }
}
