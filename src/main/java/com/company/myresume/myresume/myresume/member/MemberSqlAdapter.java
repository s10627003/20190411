package com.company.myresume.myresume.myresume.member;

import com.company.myresume.myresume.myresume.member.generated.GeneratedMemberSqlAdapter;

/**
 * The SqlAdapter for every {@link
 * com.company.myresume.myresume.myresume.member.Member} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class MemberSqlAdapter extends GeneratedMemberSqlAdapter {}