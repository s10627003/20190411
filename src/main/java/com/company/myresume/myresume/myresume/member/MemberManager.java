package com.company.myresume.myresume.myresume.member;

import com.company.myresume.myresume.myresume.member.generated.GeneratedMemberManager;

/**
 * The main interface for the manager of every {@link
 * com.company.myresume.myresume.myresume.member.Member} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface MemberManager extends GeneratedMemberManager {}